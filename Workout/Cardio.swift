//
//  Cardio.swift
//  Workout
//
//  Created by Andy Kaufmann on 4/9/17.
//  Copyright © 2017 CSE438. All rights reserved.
//

import Foundation

import UIKit

class Cardio: Excercise {
    var type:String = "Cardio"
    var activities:[Activity] = []
    
    
    func start() {
        //start the cardio timer!
    }
    
    func nextPart() {
        //just go to the next cardio excercise (if there is one!)
    }
    
    var totalTime: Int {
        var tot:Int = 0
        for a in activities {
            tot += (a.onTime + a.restTime) * a.numIntervals
        }
        tot += (activities.count * 60)
        return tot
    }
    
    func getType() -> String {
        return type
    }
    
    init(newActivities:[Activity]) {
        self.activities.append(contentsOf: newActivities)
        
    }

    
}
