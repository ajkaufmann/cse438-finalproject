//
//  Lift.swift
//  Workout
//
//  Created by Andy Kaufmann on 4/9/17.
//  Copyright © 2017 CSE438. All rights reserved.
//

import Foundation

import UIKit

class Activity {
    var type:String!
    var onTime:Int!
    var restTime:Int!
    var numIntervals:Int!
    //possibly add a weight section here!
}
