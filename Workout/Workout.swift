//
//  Workout.swift
//  Workout
//
//  Created by Andy Kaufmann on 4/9/17.
//  Copyright © 2017 CSE438. All rights reserved.
//

import Foundation

import UIKit

class Workout : Excercise {
    var type:String = "General Workout"
    var excercises:[Excercise] = []
    
    
    func start() {
        //start the first excecise timer!
        //then this will be able to call next part and go through the excercises and do stuff with it!
        //WOOP WOOP
    }
    
    func nextPart() {
        //just go to the next excercise (if there is one!)
    }
    
    var totalTime: Int {
        var tot:Int = 0
        for a in excercises {
            tot += a.totalTime
        }
        tot += (excercises.count * 150)
        return tot
    }
    
    func getType() -> String {
        return type
    }
    
    init(newExcercises:[Excercise]) {
        self.excercises.append(contentsOf: newExcercises)
    }
}
