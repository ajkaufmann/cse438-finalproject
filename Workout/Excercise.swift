//
//  Excercise.swift
//  Workout
//
//  Created by Andy Kaufmann on 4/9/17.
//  Copyright © 2017 CSE438. All rights reserved.
//

import Foundation

import UIKit

protocol Excercise {
    func start()
    var totalTime:Int { get }
    func getType() -> String
    func nextPart()
}
