//
//  Weight.swift
//  Workout
//
//  Created by Andy Kaufmann on 4/9/17.
//  Copyright © 2017 CSE438. All rights reserved.
//

import Foundation

import UIKit

class Weight: Excercise {
    var lifts:[Activity] = []
    var type:String = "Weight Lifting"
    var curLift:Int = 0
    
    func start() {
        //start the first lift timer!
    }
    
    func nextPart() {
        //just go to the next lift
    }
    
    var totalTime: Int {
        var tot:Int = 0
        for l in lifts {
            tot += (l.onTime + l.restTime) * l.numIntervals
        }
        tot += (lifts.count * 60)
        return tot
    }
    
    func getType() -> String {
        return type
    }
    
    init(newLifts:[Activity]) {
        self.lifts.append(contentsOf: newLifts)
    }
   
}
